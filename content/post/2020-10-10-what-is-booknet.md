---
title: What is BookNet?
subtitle: A book and zine first client for the LBRY network
date: 2020-10-18
author: Nick Johnson
---

## What is BookNet?


BookNet is an application for finding and sharing books and zines on the [LBRY network](https://lbry.tech/).

The goal is to provide a good user experience for using LBRY to:
  * **Search** based on title, author, file format, genre, or tags.
  * Quick **Preview** of documents without full download.
  * **Download** files.
  * **Upload** new content.

\
\
Below are some common tasks BookNet will not try to help you with, and some open source tools that will help you:

  * Good document reading experience - [Calibre](https://calibre-ebook.com/about), [KOReader](http://koreader.rocks/), [Foliate](https://johnfactotum.github.io/foliate/), [Okular](https://kde.org/applications/en/okular)
  * Library Organization/File management - [Calibre](https://calibre-ebook.com/about)
